import os
from flask import Flask
from flask import request
import pymongo
import json
from bson import json_util
from bson import objectid
import re
import json

app = Flask(__name__)
#add this so that flask doesn't swallow error messages
app.config['PROPAGATE_EXCEPTIONS'] = True
# adding a comment
# a base urls that returns all metadata records in the collection 
# (in the future we could implement paging)
@app.route("/airphoto/photos")
def photos():
    #setup the connection
    conn = pymongo.Connection(os.environ['OPENSHIFT_MONGODB_DB_URL'])
    db = conn[os.environ['OPENSHIFT_APP_NAME']]
    #query the DB for all the parkpoints
    result = db.airphoto.find()
    #Now turn the results into valid JSON
    return str(json.dumps({'results':list(result)},default=json_util.default))

# return the matching airphoto metadata document from the collection
# based on its ID supplied by the client
@app.route("/airphoto/photos/<q>")
def onePhoto(q):	
	#setup the connection
    conn = pymongo.Connection(os.environ['OPENSHIFT_MONGODB_DB_URL'])
    db = conn[os.environ['OPENSHIFT_APP_NAME']]
    # give it the id and return the cursor    
    result = db.airphoto.find({'attributes.ID': int(q)}) 
    # turn the results into valid JSON - list() converts the cursor into a list object.
    # it is then converted to JSON
    return str(json.dumps({'results' : list(result)},default=json_util.default))
	
# provide a xmin, xmax, ymin, ymax string and return a set of
# photos found withing that envelope. this require some kind
# of location property in the document - which is 'geometry {x, y}'
# in our case
@app.route("/envelope/<q>")
def envelope(q):
	vals = q.split(",")
	xmin = float(vals[0])
	xmax = float(vals[1])
	ymin = float(vals[2])
	ymax = float(vals[3])
	conn = pymongo.Connection(os.environ['OPENSHIFT_MONGODB_DB_URL'])
	db = conn[os.environ['OPENSHIFT_APP_NAME']]
	result = db.airphoto.find({ "geometry" : { "$within": { "$polygon": [ [xmin,ymax], [xmin,ymin], [xmax ,ymax], [xmax , ymin] ] } }})
	return str(json.dumps({'results' : list(result)},default=json_util.default))

@app.route("/test")
def test():
    return "<strong>status: online</strong>"
    
#need this in a scalable app so that HAProxy thinks the app is up
@app.route("/")
def home():
    return "<strong>status: online</strong>"

if __name__ == "__main__":
    app.run()
